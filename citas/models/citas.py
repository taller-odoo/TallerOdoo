# -*- coding: utf-8 -*-

from odoo import fields, models

SEXO_MASCULINO = '1'
SEXO_FEMENINO = '2'

Selection_SEXO = [
    (SEXO_MASCULINO, 'Masculino'),
    (SEXO_FEMENINO, 'Femenino'),
]


class Paciente(models.Model):
    _inherits = {'res.partner': 'partner_id'}

    _name = 'citas.paciente'

    sexo = fields.Selection(Selection_SEXO, size=1, required=True)
    dni = fields.Char('Dni', size=8, required=True)
    fecha_nacimiento = fields.Date('Fecha de Nacimiento', required=True)
