# -*- coding: utf-8 -*-
{
    'name': 'Minsa Citas - Caso Práctico',
    'version': '1.0',
    'website': 'www.minsa.gob.pe',
    'category': 'health',
    'depends': [
        'base',
        'hr',
        'crm',
    ],
    'description': 'Módulo para la gestion de Citas',
    'data': [
        'views/citas_views.xml',
        'security/ir.model.access.csv',
    ],
    'application': True,
}
