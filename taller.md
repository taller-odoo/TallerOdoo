
# Odoo v.10
Requisitos para Desarrollo
- *Ubuntu 16.04 lts*, Debian, Fedora, etc.
- *Python 2.7.x*
- *PostgreSQL 9.5*


# Build an Odoo module - Backend

https://www.odoo.com/documentation/10.0/howtos/backend.html

## Tipos de addons/modulos
- Aplicación(app):
    En general tienen menus, vistas, etc.
    Crm, Project, Point of Sales, Sales, HR, ...
    gestion_rrhh(Adaptación de HR para el Minsa), indic_gestion_odoo(Indicadores de Gestión), ...
- No aplicaciones(addon)
    En general son extenciones de otros addons o pertenecen al core del ERP
    Signup, Password Encryption, *Base*, ...
    baseapi, consultadatos, l10n_minsa, ...

## Creación de un addon/app/módulo: *nombre_app*

## Configuración de archivo citas.conf:

```
[options]
db_host = False
db_port = False
db_user = daniel
db_password = False
addons_path = /home/daniel/odoo10/odoo/addons,/home/daniel/minsadev/odoo-share,/home/daniel/minsadev,/home/daniel/minsadev/odoo-share/consultadatos
;dbfilter=^odoo10_minsa_citas
```

## Estructura básica de un addons
Se crean dentro de una carpeta *nombre_app*, principalmente se necesitan una carpeta y dos archivos(contenidos dentro de la carpeta):

*nombre_app* es una carpeta.
`__init__.py` Utilizado para inicializar paquetes de Python.
`__manifest__.py` Indica que *nombre_app* es un paquete de Odoo.

`README.md` No requerido pero necesario para documentar el addon


`__manifest__.py`

``` python
# -*- coding: utf-8 -*-
{
    'name': 'Minsa - Citas',
    'version': '1.0',
    'website': 'www.minsa.gob.pe',
    'category': 'health',
    'depends': [
        'base',
        # addon1,
        # addon2,
        # ...
    ],
    'external_dependencies': {
        'python': [
            # 'mpi_client'
            # 'cita_client'
            # 'serial'
            # 'ldap'
        ]
    },
    'description': 'Módulo para la gestion de Citas',
    'data': [
        # security/ir.model.acces.csv',
        # 'views/res_partner.xml',,
        # 'views/views.xml',
        # 'data/script.sql',
    ],
    'application': True,
}

```

`__init__.py`
``` python

```

`README.md`
```
# Gestion de Citas - Caso Praćtico

Módulo para la Gestión de Citas
```


# Caso Practico 
## Parte 01
Se requiere una aplicación *citas* para gestión de pacientes que permita:

- Mostrar como menú principal "Citas"
- Mostrar el listado de Pacientes"
- Registrar la información básica del paciente como nombre completo(apellidos y nombres), celular, email, fotografia, *sexo*, *dni*, *fecha de nacimiento*.

### Desarrollo Básico
`__init__.py`
``` python

```

`__manifest__.py`
``` python
# -*- coding: utf-8 -*-
{
    'name': 'Minsa Citas - Caso Práctico',
    'version': '1.0',
    'website': 'www.minsa.gob.pe',
    'category': 'health',
    'depends': [
        'base',
    ],
    'description': 'Módulo para la gestion de Citas',
    'data': [
        'views/citas_views.xml',
    ],
    'application': True,
}

```

`views/citas_views.xml`
``` xml
<?xml version="1.0" encoding="utf-8"?>
<odoo>
    <data>
    </data>
</odoo>
```

### Archivos *Extra* para la Validación de Código
Van en la raiz del addon
`.gitignore`

```
.idea/
*.pyc
*.swp
```

`.gitlab-ci.yml`

```
image: python:2.7
lint:
    stage: test
    script:
        - pip install -r requirements_test.txt
        - flake8 .
```

`requirements_test.txt`

```
flake8==3.0.4
pep8-naming==0.4.1
```

`setup.cfg`

```
[flake8]
ignore = E501
exclude = .git
max-line-length = 120
```

Se instala por única vez `flake8`
```bash
pip install flake8
```


# Herencia de Modelos
![Tipos de Herencia](https://www.odoo.com/documentation/8.0/_images/inheritance_methods.png)

## Traditional Inheritance

### Class inheritance
```python
class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    numero_documento = fields.Char('Dni', size=8)
    tipo_documento = fields.Selection(
        [('06', 'Pasaporte'),
         ('01', 'DNI')],
        'Tipo de Documento')
    numero_celular = fields.Char('Dni', size=12)
```

- Se agrega/modifica atributos/metodos,características.
- La nueva clase es compatible con las vistas existentes
- *No* se crea una nueva tabla, *se almacena en la misma tabla res_partner*

Se tendría en la tabla *res_partner*

id | name | tipo_documento | numero_documento | ...
----|----|----|----|----
100 | Juan Perez | 01 | 08123456 | ...


### Prototype inheritance

```python
class Paciente(models.Model):
    _name = 'citas.paciente'
    _inherit = 'res.partner'

    tipo_documento = None

    @api.model
    def create(self, vals):
        numero_celular = vals.get('numero_celular')
        if numero_celular and len(numero_celular) == 9:
            vals['numero_celular'] = u'51-{}'.format(numero_celular)
        return super(Paciente, self).create(vals)

```

- Se crea una nueva clase con las atributos/funcionalidades agregadas y/o modificadas.
- La nueva clase *ignorará* las vistas existentes
- Se almacena en una nueva tabla `citas_paciente`

Se tendría en la tabla *res_partner*

 id | name | tipo_documento | numero_documento | ...
----|----|----|----|----
100 | Juan Perez | 01 | 08123456 | ...


Se tendría en la tabla *citas_paciente*

id | name | numero_documento | ...
----|----|----|----
80 | Juan Perez | 08123456 | ...

## Delegation inheritance

```python
class Paciente(models.Model):
    _name = 'citas.paciente'
    _inherits = {'res.partner': 'partner_id'}

    tiene_sis = fields.Boolean(u'Tiene SIS')
    numero_sis = fields.Chat(u'Número SIS')

```

- Es posible la "Herencia Multiple"
- La nueva clase *ignorará* las vistas existentes
- Se almacena en una nueva tabla `citas_paciente`

Se tendría en la tabla *res_partner*

id | name | tipo_documento | numero_documento | ....
----|----|----|----|-----
100 | Juan Perez | 01 | 08123456 | ....

Se tendría en la tabl *citas_paciente*

id | partner_id | tiene_sis | numero_sis
----|----|----|----
10 | 100 | True | 123456


### Desarrollo del Modelo Paciente

`__init__.py`

``` python
from . import models  # noqa

```

`models/__init__.py`

``` python
from . import citas  # noqa

```

`models/citas.py`

``` python
# -*- coding: utf-8 -*-

from odoo import fields, models

SEXO_MASCULINO = '1'
SEXO_FEMENINO = '2'

SELECTION_SEXO = [
    (SEXO_MASCULINO, 'Masculino'),
    (SEXO_FEMENINO, 'Femenino'),
]


class Paciente(models.Model):
    _inherits = {'res.partner': 'partner_id'}

    _name = 'citas.paciente'

    sexo = fields.Selection(SELECTION_SEXO, size=1, required=True)
    dni = fields.Char('Dni', size=8, required=True)
    fecha_nacimiento = fields.Date('Fecha de Nacimiento', required=True)

```

Probamos que no hay errores de Flake 8

```bash
flake8
```

Recargar el modulo ya que se ha *creado/moficado* un modelo del addon/módulo

```bash
./odoo-bin -c odoo.conf -u citas
```

Luego de recargar se observa en el log:
```
2018-04-19 04:37:12,878 7110 INFO odoo10_minsa_sghce_DB_SJL_DEMO odoo.models: Missing many2one field definition for _inherits reference "partner_id" in "citas.paciente", using default one.
2018-04-19 04:37:13,007 7110 WARNING odoo10_minsa_sghce_DB_SJL_DEMO odoo.modules.loading: The model citas.paciente has no access rules, consider adding one. E.g. access_citas_paciente,access_citas_paciente,model_citas_paciente,,1,0,0,0
```

Se debe agregar el archivo:

`security/ir.model.access.csv`

```
id,name,model_id:id,group_id:id,perm_read,perm_write,perm_create,perm_unlink
access_citas_paciente,access_citas_paciente,model_citas_paciente,,1,0,0,0
```

Y modificar el `__manifest__.py` en la sección *data*:

``` python
{
    'data': [
        'security/ir.model.access.csv',
    ],
}

```

Nuevamente recargamos el módulo ya que se ha modificado el `__manfifest__.py` y/o agregado/modificado un archivo dentro de *data*.

```bash
./odoo-bin -c odoo.conf -u citas
```

### Desarrollo de las Vistas de Paciente y Menú de la Aplicación

`views/citas_views.xml`
``` xml
<?xml version="1.0" encoding="utf-8"?>
<odoo>
    <data>

        <record id="paciente_view_search" model="ir.ui.view">
            <field name="name">citas.paciente.search</field>
            <field name="model">citas.paciente</field>
            <field name="arch" type="xml">
                <search>
                    <field name="name" string="Apellidos y Nombres"/>
                    <field name="dni"/>
                </search>
            </field>
        </record>

        <record id="paciente_view_tree" model="ir.ui.view">
            <field name="name">citas.paciente.tree</field>
            <field name="model">citas.paciente</field>
            <field name="arch" type="xml">
                <tree>
                    <field name="name" string="Apellidos y Nombres"/>
                    <field name="dni"/>
                    <field name="fecha_nacimiento"/>
                    <field name="sexo"/>
                </tree>
            </field>
        </record>

        <record id="paciente_view_form" model="ir.ui.view">
            <field name="name">citas.paciente.form</field>
            <field name="model">citas.paciente</field>
            <field name="type">form</field>
            <field name="arch" type="xml">
                <form>
                    <sheet>
                        <group>
                            <field name="name" string="Apellidos y Nombres"/>
                            <field name="dni"/>
                            <field name="fecha_nacimiento"/>
                            <field name="sexo"/>
                        </group>
                    </sheet>
                </form>
            </field>
        </record>

        <record id="paciente_action" model="ir.actions.act_window">
            <field name="name">Pacientes</field>
            <field name="res_model">citas.paciente</field>
            <field name="view_type">form</field>
            <field name="view_mode">tree,form</field>
            <field name="domain">[]</field>
        </record>


        <menuitem id="citas_app" name="Citas" sequence="10"/>
        <menuitem id="pacientes" action="paciente_action" parent="citas_app" />

    </data>
</odoo>
```

Recargamos el modulo

## Preguntas:
1. ¿Qué estructura se espera en las tablas citas_paciente y res_partner?
2. ¿Qúe información almacena cada una?
3. ¿Porque sucede 1. y 2.?


## Parte 02
- Agregar la fotografía del ciudadano
- Agregar dos tipos de restricciones:
    - El número dni no se puede repetir, debe contener solo digitos y longitud 8.
    - La Fecha de nacimiento no puede ser del dia siguiente, no debe permitir guardar una fecha de nacimiento inválida.
- Agregar seguimiento a los valores del formulario.
- Agrege un atributo *edad* del paciente (considere edad = año actual - año de nacimiento)


## Parte 03
### Conexión a un servicio externo MPI
Cuando se modifique el dni se debe obtener la información del paciente desde un servicio externo.

### Pasos:
- Configurar __manifest__.py y agregar la dependencia al servicio de MPI
- configurar los parámetros del sistema para *mpi_api_host* y *mpi_api_token* del servicio de mpi.
- Agregar un metodo *_onchange_dni* para que pueda hacer la consulta y mostrar los datos del ciudadano en el formulario.
- Solamente debe guardar el formulario luego que se ha hecho la consulta a mpi.


## Parte 04
- Análogamente al modelo del Paciente, crear el modelo Médico partiendo del modelo Empleado(hr.employee).
- Crear las vistas del Médico heredando las vistas del empleado sin que estas se modifiquen.
- Crear el item de menu Médicos.
- Obtener los datos del Médico desde una consulta a MPI.
- Agrege especialidades del medico
- Agrege tipo_documento al Empleado
- En el formulario de la Especilidad mostrar los Medicos de la especialidad.


## Parte 05
- Crear el modelo ProgramacionEspecialista que contiene Día, Médico, Especialidad, solo se aceptan hasta 10 citas(se usar como parametro de sistema) por ProgramacionEspecialista, cada médico puede tener un número determinado de citas acicionales(2 por defecto)
- Crear Modelo Cita que almacenará Medico Especialista, Fecha, Paciente.
- En el formulario del paciente mostrar sus citas.
- En el formulario del medico mostrar sus ProgramacionEspecialista de los proximos 7 dias.

